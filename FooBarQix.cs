using System;
using System.Collections;
using System.Collections.Generic;

public class FooBarQix
{
    public FooBarQix()
    {
    }

    public string SampleNumber(int number)
    {
        string resultedString = "";
        if (ShouldSubsituteTheNumber(number))
        {
            resultedString += SampleDivisibles(number);
            resultedString += SampleDigits(number);
        }
        else
        {
            resultedString = SubstituteADigitForAChar(number,'0','*');
        }
        return resultedString;

    }

    private string SubstituteADigitForAChar(int number, char digitToSubstitue, char substitute)
    {
        string resultedString = "";
        foreach(var digit in number.ToString())
        {
            if (digit == digitToSubstitue)
                resultedString += substitute.ToString();
            else
                resultedString += digit.ToString();
        }
        return resultedString;
    }

    private string SampleDigits(int number)
    {
        string resultedString = "";
        foreach (var digit in number.ToString())
        {
            resultedString += SubstituteDigit(digit);
        }

        return resultedString;
    }

    private string SubstituteDigit(char digit)
    {
        switch (digit)
        {
            case '0':
                return "*";
            case '3':
                return "Foo";
            case '5':
                return "Bar";
            case '7':
                return "Qix";
            default:
                return "";
        }
    }
  
    private string SampleDivisibles(int number)
    {
        string resultedString = "";
        if (IsDivisibleBy(number, 3))
            resultedString += "Foo";
        if (IsDivisibleBy(number, 5))
            resultedString += "Bar";
        if (IsDivisibleBy(number, 7))
            resultedString += "Qix";
        return resultedString;
    }

    public bool IsDivisibleBy(int dividend, int divisor)
    {
            return dividend % divisor == 0;
    }

    public bool ShouldSubsituteTheNumber(int number)
    {
        if (IsDivisibleBy(number, 3))
            return true;
        if (IsDivisibleBy(number, 5))
           return true;
        if (IsDivisibleBy(number, 7))
           return true;
        if (ContainsDigit(number,3))
            return true;
        if (ContainsDigit(number,5))
            return true;
        if (ContainsDigit(number,7))
            return true;
        return false;
    }

    public bool ContainsDigit(int number, int digit)
    {
        string numberStr = number.ToString();
        string digitStr = digit.ToString();
        if (numberStr.Contains(digitStr))
            return true;
        return false;
    }
}
