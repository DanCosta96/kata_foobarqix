using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

[TestFixture]
public class FooBarQixTests
{
    public FooBarQix fooBarQuix;
    public string ResultedString;

    [SetUp]
    public void SetUp()
    {
        GivenAFooBarQuixClass();
    }

    [Test]
    public void ANumberDivisibleBy3WriteFoo()
    {
        WhenSampleNumber(6);

        ThenResultedStringIs("Foo");
    }

    [Test]
    public void ANumberDivisibleBy5WriteBar()
    {
        WhenSampleNumber(10);

        ThenResultedStringIs("Bar*");
    }

    [Test]
    public void ANumberDivisibleBy7WriteQix()
    {
        WhenSampleNumber(14);

        ThenResultedStringIs("Qix");
    }

    [Test]
    public void ANumberDivisibleBy3And5WriteFooBar()
    {
        WhenSampleNumber(60);

        ThenResultedStringIs("FooBar*");
    }

    [Test]
    public void ANumberDivisibleBy5And7WriteBarQix()
    {
        WhenSampleNumber(140);

        ThenResultedStringIs("BarQix*");
    }

    [Test]
    public void ANumberThatContains3WriteFoo()
    {
        WhenSampleNumber(13);

        ThenResultedStringIs("Foo");
    }

    [Test]
    public void ANumberThatContains5WriteBar()
    {
        WhenSampleNumber(52);

        ThenResultedStringIs("Bar");
    }

    [Test]
    public void ANumberTharContains7WriteQix()
    {
        WhenSampleNumber(17);

        ThenResultedStringIs("Qix");
    }

    [Test]
    public void KeepTraceOfDigit0InNumberRePlacedWithAsterisk()
    {
        WhenSampleNumber(1004);

        ThenResultedStringIs("1**4");
    }

    [Test]
    [TestCase(33, "FooFooFoo")]
    [TestCase(357, "FooQixFooBarQix")]
    [TestCase(753, "FooQixBarFoo")]
    [TestCase(105, "FooBarQix*Bar")]
    [TestCase(101, "1*1")]
    public void NumberCases(int number, string expectedString)
    {
        WhenSampleNumber(number);

        ThenResultedStringIs(expectedString);
    }

    public void GivenAFooBarQuixClass()
    {
        fooBarQuix = new FooBarQix();
    }

    public void WhenSampleNumber(int number)
    {
        ResultedString = fooBarQuix.SampleNumber(number);
    }

    public void ThenResultedStringIs(string expectedString)
    {
        Assert.That(ResultedString, Is.EqualTo(expectedString));
    }
}
